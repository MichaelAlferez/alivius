<?php   
   
require('../../../CONNECTION/SECURITY/conex.php');
//session_start();
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);

//Exportar datos de php a Excel
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

date_default_timezone_set("America/Bogota");
$hora = date('Y-m-d');

$tipo = base64_decode($_GET[MD5('tipo')]);
if($_POST['reporte_ruta'] != ''){
     
	 if($_POST['reporte_ruta']== 'FECHA HOY'){
	 
	 header("content-disposition: attachment;filename=reportsHoyRuta.xls");
	 $hoy = " WHERE  date(A.`fecha_ruta`) = '".$hora."'";
	 $between = '';
	 
	 }elseif($_POST['reporte_ruta'] == 'ENTRE FECHAS'){
	 
	 header("content-disposition: attachment;filename=reportsEntreFechasRuta.xls");
	 $primeraFecha = $_POST['primerafechaR'];
	 $segundaFecha = $_POST['segundafechaR'];
	 
	 $between = "WHERE A.`fecha_ruta` BETWEEN '".$primeraFecha."' AND '".$segundaFecha."'";
	 $hoy = '';
	 
	 }elseif($_POST['reporte_ruta'] == 'TODO'){
	 
	 header("content-disposition: attachment;filename=reportsTodoRuta.xls");
	 $hoy = '';
	 $between = '';
	 }
    
}
  
  $consulta_paciente=mysqli_query($conex,"SELECT * FROM `asesor_ruta` AS A LEFT JOIN pdv_farmacia AS B ON A.id_pdv = B.id_pdv LEFT JOIN user AS C ON A.id_asesor = C.id_user ".$hoy."".$between);
  echo mysqli_error($conex);

?>
 
<table border="1px" bordercolor="#000000">
      <tr style="font-weight:bold; text-transform:uppercase; height:25; padding:3px">
          
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >ESTADO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >USUARIO ASIGNADO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >FECHA RUTA</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >COD</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >REGION</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >DEPARTAMENTO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CIUDAD</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CAM</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >NOMBRE PDV</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >DIRECCI&Oacute;N</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >TELEFONO</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CADENA PDV</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >NOMBRE REGENTE</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >N&ordm; REG.</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CONVENIO</th>
          <th class="botones" style="background-color:#23697b; color: #ffffff;" >CAT.</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >VISITA</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CANAL</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >TIPO DE GESTION</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >CODIFICADO</th>
		  <th class="botones" style="background-color:#23697b; color: #ffffff;" >VECTORIZADO</th>
		  
	      
      </tr>
	  <?php
      while ($fila1 = mysqli_fetch_array($consulta_paciente))
      { 
      ?>
          <tr align="center" style="height:25px;">
          	  
			  <td><?php if($fila1['estado'] == '1'){ echo "SIN GESTIONAR";}elseif($fila1['estado']== '0'){echo "GESTIONADO"; }elseif($fila1['estado'] == '2'){echo "GESTIONADO EN MODIFICACIÓN"; } ?></td>
              <td><?php echo $fila1['names']." ".$fila1['surnames']; ?></td>
			  <td><?php echo $fila1['fecha_ruta']; ?></td>
			  <td><?php echo $fila1['cod_pdv']; ?></td>
			  <td><?php echo utf8_decode($fila1['region']); ?></td>
			  <td><?php echo utf8_decode($fila1['departamento_pdv']); ?></td>
			  <td><?php echo utf8_decode($fila1['ciudad_pdv']); ?></td>
			  <td><?php echo utf8_decode($fila1['cam']); ?></td>
			  <td><?php echo utf8_decode($fila1['nombre_pdv']); ?></td>
		      <td><?php echo utf8_decode($fila1['direccion_pdv']);?></td>
			  <td><?php echo utf8_decode($fila1['telefono']);?></td>
			  <td><?php echo utf8_decode($fila1['cadena_pdv']);?></td>
			  <td><?php echo utf8_decode($fila1['nombre_regente']);?></td>
			  <td><?php echo $fila1['num_regetente'];?></td>
			  <td><?php echo utf8_decode($fila1['convenio']);?></td>
			  <td><?php echo $fila1['categoria']?></td>
			  <td><?php echo $fila1['visita']?></td>
			  <td><?php echo $fila1['canal_pdv']?></td>
			  <td><?php echo utf8_decode($fila1['tipo_gestion']);?></td>
			  <td><?php echo $fila1['codificado'];?></td>
			  <td><?php echo $fila1['vectorizado'];?></td>
			               
			  </tr>
              <?php
			 
      }
      ?>
 </table>
