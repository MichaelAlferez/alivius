<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php

require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

$id_pap = $_GET['id_pap'];
$sqlp = mysqli_query($conex, "SELECT *,E.estado AS estadoa FROM paciente as A LEFT JOIN gestion as G ON A.id_pap = G.id_paciente LEFT JOIN estado_registry AS E ON A.id_pap = E.id_paciente WHERE A.id_pap = $id_pap");

while ($mostrar = mysqli_fetch_array($sqlp)) {
 // $id_pap = $mostrar['id_pap'];
  $nombre = $mostrar['nombre'];
  $apellido = $mostrar['apellido'];
  $cedula = $mostrar['documento'];
  $telefono = $mostrar['telefono'];
  $direccion = $mostrar['direccion'];
  $ciudad = $mostrar['ciudad'];
  $departamento = $mostrar['departamento'];
  $ips = $mostrar['ips'];
  $cie10 = $mostrar['cie10'];
  $diagnostico = $mostrar['diagnostico'];
  $nom_acu = $mostrar['nom_acu'];
  $parent_acu = $mostrar['parent_acu'];
  $tel_acu = $mostrar['tel_acu'];
  $descripcion = $mostrar['descripcion'];
  $fech_proxgestion = $mostrar['fech_proxgestion'];
  $estadoa = $mostrar['estadoa'];
  $causal = $mostrar['causal'];

}

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Encuesta</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../../../DESIGN/assets/demo/material-dashboard.min.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <script src="../../../DESIGN/JS/jquery-3.5.1.min.js"></script>
  <script src="../../../DESIGN/JS/sweetalert.js"></script>
<script>
function activar_solicitud() {

if ($('#activar_tol').is(":checked")) {
  swal("Esta activo", "Se enviara una solicitud de Cambio","success");


 $("#activar_nom").attr('disabled', false);
 $("#activar_apell").attr('disabled', false);
 $("#activar_cedu").attr('disabled', false);
 $("#activar_ips").attr('disabled', false);
 $("#activar_cie10").attr('disabled', false);
 $("#activar_diag").attr('disabled', false);
 $("#activar_est").attr('disabled', false);
 

}else if ($('#activar_tol').is(":not(:checked)")) {

  swal("Desactivo","la Solicitud de Cambio","warning");
  
  $("#activar_nom").attr('disabled', true);
  $("#activar_apell").attr('disabled', true);
 $("#activar_cedu").attr('disabled', true);
 $("#activar_ips").attr('disabled', true);
 $("#activar_cie10").attr('disabled', true);
 $("#activar_diag").attr('disabled', true);
 $("#activar_est").attr('disabled', true);
 $("#nombre").attr('disabled', true); 
 $("#apellido").attr('disabled', true);
 $("#cedula").attr('disabled', true);
 $("#ips").attr('disabled', true);
 $("#cie10").attr('disabled', true);
 $("#diagnostico").attr('disabled', true);

 
}


}

function habilitar_campo() {
  
  if ($('#activar_nom').is(":checked")) {
        $("#nombre").attr('disabled', false); 
          }else if ($('#nombre').is(":not(:checked)")) {
        $("#nombre").attr('disabled', true); 
  }
  if ($('#activar_apell').is(":checked")) {
        $("#apellido").attr('disabled', false); 
          }else if ($('#activar_apell').is(":not(:checked)")) {
        $("#apellido").attr('disabled', true); 
  }
  if ($('#activar_cedu').is(":checked")) {
        $("#cedula").attr('disabled', false); 
          }else if ($('#activar_cedu').is(":not(:checked)")) {
        $("#cedula").attr('disabled', true); 
  }
  if ($('#activar_ips').is(":checked")) {
        $("#ips").attr('disabled', false); 
          }else if ($('#activar_ips').is(":not(:checked)")) {
        $("#ips").attr('disabled', true); 
  }
  if ($('#activar_cie10').is(":checked")) {
        $("#cie10").attr('disabled', false); 
          }else if ($('#activar_cie10').is(":not(:checked)")) {
        $("#cie10").attr('disabled', true); 
  }
  if ($('#activar_diag').is(":checked")) {
        $("#diagnostico").attr('disabled', false); 
          }else if ($('#activar_diag').is(":not(:checked)")) {
        $("#diagnostico").attr('disabled', true); 
  }


}

function causalval(){
		var causal1 = $('#causal1').val();
    var estadoes = $('#estadoes').val();
    if (causal1 == 'Nuevo') {
      $("#fechProxGestion1").css('display', 'block');
      $('#fechProxGestion1').attr('required', 'required');
      $("#fechProxGestion2").css('display', 'none');
      $('#fechProxGestion2').removeAttr('required', 'required');
      $('#fechProxGestion1').attr('readonly', 'readonly');
      $('#fechProxGestion1').removeAttr('disabled', 'disabled');
      $('#fechProxGestion2').attr('disabled', 'disabled');
			
		} else if (estadoes != 'Retiro') {
      $("#fechProxGestion1").css('display', 'none');
      $('#fechProxGestion1').removeAttr('required', 'required');
      $("#fechProxGestion2").css('display', 'block');
      $('#fechProxGestion2').attr('required', 'required');
      $('#fechProxGestion1').attr('disabled', 'disabled');
      $('#fechProxGestion1').removeAttr('readonly', 'readonly');
      $('#fechProxGestion2').removeAttr('disabled', 'disabled');
		}
		
}
    function traerdata() {

      var departamento1 = $('#departamento1').val();
      var estado = '4';
      var ciudad_ar = <?php print '"'.$ciudad.'"'; ?>; 
    
      //solo usa para activar el ajax
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
        data: {
          departamento1: departamento1,
          estado: estado,
          ciudad_ar: ciudad_ar
        },
        type: 'post',
        beforesend: function() {},
        success: function(data) {
          $('#ciudad').html(data);
          
        } 
      });
 
    }

    

    function traerdata2() {
      var estado = '3';
      var departamento_ar = <?php print '"'.$departamento.'"'; ?>;
 
      //solo usa para activar el ajax
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_ciudad.php',
        data: {
          estado: estado,
          departamento_ar: departamento_ar

        },
        type: 'post',
        beforesend: function() {},
        success: function(data) {
          $('#departamento').html(data);
        }
      });
    }

    function traerEstado() {
      var estadoe = '3';

      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
        data: {
           estadoe: estadoe
        },
        type: 'post',
        beforeSend: function() {},
        success: function(data) {
          $('#estado').html(data);
        }
      });
    }

    function traercausal() {
      var estadoes = $('#estadoes').val();
      if(estadoes == 'Retiro'){
      $("#fechProxGestion1").css('display', 'none');
      $("#fechProxGestion2").css('display', 'none');


    }
     //$('#fechlit').attr("value","hola1");
      var estadoe = '4';
      var estadoes = $('#estadoes').val();

      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_causal.php',
        data: {
          estadoe: estadoe,
          estadoes: estadoes
        },
        type: 'post',
        beforeSend: function() {},
        success: function(data) {
          $('#causal').html(data);
        }
      });
    }
  </script>
  <script>
   traerdata2();
   traerEstado();
    
  </script>


</head>

<body class="">
  <div class="wrapper">
  <?php

$idrol = $_SESSION['id_loginrol'];

if ($idrol == 1) {
 require("../../DROPDOWN/admin_menu.php");
} else {
  require("../../DROPDOWN/adviser_menu.php");
}
?>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a href="tabla_seguimiento.php">
              <button type="button" class="btn btn-primary btn-fab btn-fab-mini btn-round"><span class="material-icons">reply</span></button>
            </a>
            <a style="color:#333333;" class="navbar-brand" href="javascript:;">
              <?php echo 'PAP ' . $id_pap; date_default_timezone_set("America/Bogota");
                              $dia = date('Y-m-d'); if($fech_proxgestion < $dia){ echo  '<span style="color:red;"> '.$fech_proxgestion. '</span>';
                                $diff = abs(strtotime($dia) - strtotime($fech_proxgestion));
                                $years = floor($diff / (365*60*60*24));
                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                echo " ya han pasado ".$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24))." D&iacute;as"; }
                                    elseif ($fech_proxgestion == $dia) {  echo  ' <span style="color:green;">'.$fech_proxgestion.'</span>'; }
                                    ?>
            </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pendiente Punto BAR004</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR005</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR006</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR007</a>
                  <a class="dropdown-item" href="#">Pendiente Punto BAR008</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Perfil</a>
                  <a class="dropdown-item" href="#">Configuraci&oacute;n</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../../CONNECTION/SECURITY/destroy.php">Cerrar Sesi&oacute;n</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">REGISTRO SEGUIMIENTO </h4>
              </div>
              <div class="card-body">
              <div class="togglebutton">
  <label>
    <input type="checkbox" name="activar_tol" id="activar_tol" onclick="activar_solicitud()" ><!----Activador total--->
      <span class="toggle"></span>
       Activa si deseas solicitar cambio
  </label>
</div>
                <!--------------------------------------- FORM --------------------------------------->
                <form method="post" enctype="multipart/form-data">

                  <div class="form-row">
                    <!--Inicio del Form -->

                    <!-- INFORMACION DEL PUNTO  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">

                        </a>
                      </h5>
                    </div>
                    <div class="col-md-3">
                      <label for="inputEmail4">Nombre<span class="text-danger">* </span><input type="checkbox" name="activar_nom" id="activar_nom" onclick="habilitar_campo()" disabled ><!----Solicitar---></label>
                      <input type="text" class="form-control" placeholder="" name="nombre" id="nombre" maxlength="50" value="<?php echo $nombre; ?>" disabled>
                      <br>

                    </div>

                    <div class="col-md-3">
                      <label for="inputPassword4">Apellido<span class="text-danger">* </span><input type="checkbox" name="activar_apell" id="activar_apell" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <input type="text" class="form-control" placeholder="" name="apellido" id="apellido" maxlength="50" value="<?php echo $apellido; ?>" disabled>
                    </div>

                    <div class="col-md-3">
                      <label for="inputAddress">Cedula<span class="text-danger">* </span><input type="checkbox" name="activar_cedu" id="activar_cedu" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <input type="text" class="form-control" placeholder="" name="cedula" id="cedula" maxlength="20" value="<?php echo $cedula; ?>" disabled>
                    </div>
                    <div class="col-md-3">
                      <label for="inputAddress2">Telefono<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="telefono" id="telefono" value="<?php echo $telefono; ?>" maxlength="20">
                    </div>

                    <div class="col-md-6">
                      <label for="inputCity">Direccion<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="direccion" id="direccion" maxlength="100" value="<?php echo $direccion; ?>">
                    </div>

                    <div class="col-md-3">
                      <label for="inputCity">Departamento<span class="text-danger">*</span></label>
                      <span name="departamento" id="departamento">
                      </span>
                    </div>

                    <div class="col-md-3">
                      <label for="inputCity">Ciudad<span class="text-danger">*</span></label>
                      <span name="ciudad" id="ciudad" ><select id="ciudad_ar" class="form-control " style="background-color: #fff; color: black;" data-style="btn btn-link">
                      <option><?php print $ciudad; ?></option>
                      </select>
                      </span>
                    </div>
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">IPS<span class="text-danger">* </span><input type="checkbox" name="activar_ips" id="activar_ips" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <input type="text" class="form-control" placeholder="" name="ips" id="ips" maxlength="50" value="<?php echo $ips; ?>" disabled>
                    </div>
                    <div class="col-md-3">
                      <label for="inputCity">Cie10<span class="text-danger">* </span><input type="checkbox" name="activar_cie10" id="activar_cie10" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <input type="text" class="form-control" placeholder="" name="cie10" id="cie10" maxlength="6" value="<?php echo $cie10; ?>" disabled>
                    </div>
                    <div class="col-md-5">
                      <label for="exampleFormControlTextarea1">Diagnostico <span class="text-danger">*</span> <input type="checkbox" name="activar_diag" id="activar_diag" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <input type="text" class="form-control" name="diagnostico" id="diagnostico" maxlength="80" value="<?php echo $diagnostico; ?>" disabled>
                    </div>

                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="inputCity">Nombre Cuidador<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="nombreCuid" id="nombreCuid" maxlength="80" value="<?php echo $nom_acu; ?>">
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Parentesco<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="parentesco" id="parentesco" maxlength="20" value="<?php echo $parent_acu; ?>">
                    </div>
                    <div class="col-md-4">
                      <label for="inputCity">Telefono Acudiente<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" placeholder="" name="telAcudiente" id="telAcudiente" maxlength="20" value="<?php echo $tel_acu; ?>">
                    </div>
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">

                        </a>
                      </h5>
                    </div>
                    <!-- FIN DE INFORMACION DEL PUNTO  -->
                    <!-- INICIO DE PREGUNTAS  -->
                    <div class="col-md-12">
                      <h5 class="mb-12">
                        <a class="collapsed" data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                        </a>
                      </h5>
                    </div>

                    <div class="col-md-4">
                      <label for="disponibilidad">Estado<span class="text-danger">* </span><input type="checkbox" name="activar_est" id="activar_est" onclick="habilitar_campo()" disabled><!----Solicitar---></label>
                      <select class="form-control selectpicker" data-style="btn btn-link" name="estado" id="estado" disabled>
                        <option><?php echo $estadoa;  ?></option>
                        
                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="modalidad">Causal<span class="text-danger">* </span></label>
                      <select class="form-control selectpicker" data-style="btn btn-link" name="causal" id="causal" disabled>
                        <option value=""><?php echo $causal; ?></option>
                        </select>
                    </div>
                    <div class="col-md-12">
                      <p></p>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-fab btn-fab-mini btn-round" data-toggle="modal" data-target="#exampleModal">
                          <i class="material-icons">remove_red_eye</i>

                        </button>

                        <!-- Modal -->
                        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Historico de Gestiones</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="material-datatables">
                                  <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <span id="banertabla">
                                          <table id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                                            <thead>
                                              <tr role="row">

                                                <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1"  aria-label="Position: activate to sort column ascending">Fech. Prog.</th>
                                                <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" style="width: 50px;" >Descripci&oacute;n</th>
                                                <th class="sorting sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1" aria-label="PROXIMO">Fecha registro</th>
                                                <th class="sorting" tabindex="0" aria-controls="datatables" rowspan="1" colspan="1"  aria-label="Position: activate to sort column ascending">Usuario</th>
                                              </tr>
                                            </thead>

                                            <tfoot>
                                              <tr>
                                                <th rowspan="1" colspan="1">Cod</th>
                                                <th rowspan="1" colspan="1">Producto</th>
                                                <th rowspan="1" colspan="1">Proximo Contacto</th>
                                                <th rowspan="1" colspan="1">Telefono</th>
                                              </tr>
                                            </tfoot>
                                            <tbody>
                                              <?php
                                              date_default_timezone_set("America/Bogota");
                                              $dia = date('Y-m-d');

                                              $sql = "SELECT * FROM `gestion` WHERE `id_paciente` = '$id_pap'";

                                              $query = mysqli_query($conex, $sql);

                                              while ($row = mysqli_fetch_array($query)) {
                                              ?>
                                                <tr height="20">
                                                  <td><?= $row['fech_proxgestion'] ?></td>
                                                  <td><?= $row['fecha_reg'] ?></td>
                                                  <td><?= $row['id_user'] ?></td>
                                                  <td><?= $row['descripcion'] ?></td>
                                                </tr>

                                              <?php } ?>
                                            </tbody>


                                          </table>
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card ">
                        <div class="card-header card-header-rose card-header-text">
                          <div class="card-icon">
                            <i class="material-icons">today</i>
                          </div>
                          <h4 class="card-title">Fecha de gesti&oacute;n</h4>
                        </div>
                        <div class="card-body ">
                          <div class="form-group bmd-form-group is-filled">
                            <input type="date" class="form-control datetimepicker" name="fechgestion" id="fechgestion" value="<?php date_default_timezone_set("America/Bogota");
                                                                                                                              echo date('Y-m-d'); ?>" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card ">
                        <div class="card-header card-header-rose card-header-text">
                          <div class="card-icon">
                            <i class="material-icons">today</i>
                          </div>
                          <h4 class="card-title">Fecha de proxima gesti&oacute;n</h4>
                        </div>
                        <div class="card-body ">
                        <div class="form-group bmd-form-group is-filled">
                          <input type="date" class="form-control datetimepicker" name="fechProxGestion" id="fechProxGestion2" min="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>" >
                            <input type="date" class="form-control datetimepicker" name="fechProxGestion" id="fechProxGestion1" value="<?php $fecha = date('Y-m-d');$nuevafecha = strtotime ( '+14 day' , strtotime ( $fecha ) ) ;$nuevafecha = date ( 'Y-m-d' , $nuevafecha );echo $nuevafecha;?>" style="display:none;">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Descripci&oacute;n</label>
                      <textarea class="form-control" rows="6" name="descripcion" id="descripcion" ></textarea>
                    </div>
                  </div>



                  <!-- FIN PREGUNTAS -->

              </div><br> <!-- Fin Form -->

              <center>
                <div class="col-md-4"><button id="enviarEncuesta" formaction="../..//FUNCTIONS/CRUD/insert_paci_new.php" type="submit" class="btn btn-success mat-raised-button mat-button-base"><i class="material-icons">verified_user</i> Enviar Informaci&oacute;n </button> </div>
              </center>
              <div class="col-md-4"></div>


              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  </div>

  <!-- end content-->


  <!-- end col-md-12 -->

  <footer class="footer">

    <div class="container-fluid">

      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear());
        </script>
        <a href="https://peoplemarketing.com/inicio/" target="_blank">People Marketing</a>
      </div>
    </div>

  </footer>

  <!--   Core JS Files   -->

  <script src="../../../DESIGN/assets/js/core/popper.min.js"></script>
  <script src="../../../DESIGN/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../../../DESIGN/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../../../DESIGN/assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../../../DESIGN/assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../../../DESIGN/assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../../../DESIGN/assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../../../DESIGN/assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../../../DESIGN/assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../../../DESIGN/assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../../../DESIGN/assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  
  <!-- Chartist JS -->
  <script src="../../../DESIGN/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../../../DESIGN/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../../DESIGN/assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->

  <!-- Script -->

  <!-- Fin de Scripts -->
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Tot"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Buscar registros",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>