<?php
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

?>
<table id="myTable">
                                        <thead class="text-white" style="background-color: #e91e63; font-size: 16px;">
                                            <tr>
                                                <th rowspan="1" colspan="1">#</th>
                                                <th rowspan="1" colspan="1">Rol</th>
                                                <th rowspan="1" colspan="1">Nombre</th>
                                                <th rowspan="1" colspan="1">Apellido</th>
                                                <th rowspan="1" colspan="1">Nombre de usuario</th>
                                                <th rowspan="1" colspan="1">Estado</th>
                                                <th class="text-center" rowspan="1" colspan="1">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            $sql = "SELECT * FROM user as u INNER JOIN userlogin as ul ON ul.id_user = u.id_user INNER JOIN user_rol as ur ON ur.id_rol= ul.id_loginrol ORDER BY u.id_user ASC";
                                            $query = mysqli_query($conex, $sql);
                                            // $activo=='activo';
                                            while ($row = mysqli_fetch_array($query)) { 
                                            ?>  
                                            <?php
                                                    $activo=['activo'];
                                                    if ($row['activo']==1) {
                                                        $activo='Activo';
                                                    } else {
                                                        $activo='Desactivo';
                                                    }
                                                    
                                                    ?>
                                                <tr>
                                                    <td><?= $row['id_user'] ?></td>
                                                    <td><?= $row['nombre_rol'] ?></td>
                                                    <td width="80"><?php if ($row['activo'] == '0' ) {
                                                        echo '<span style="color:red; background-color:transparent;">' . $row['names'] . ' </span>';
                                                        } elseif ($row['activo'] == '1') { /* Sin Gestionar*/
                                                        echo '<span style="color:#4caf50; background-color:transparent">' . $row['names'] . ' </span>';
                                                        }elseif ($row['activo'] == '2') { /* Sin Gestionar*/
                                                            echo '<span style="color:#e91e63; background-color:transparent">' . $row['names'] . ' </span>';
                                                            }
                                                         ?>
                                  </td>
                                                    <td><?= $row['surnames'] ?></td>
                                                    <td><?= $row['name_user'] ?></td>
                                                    <td><?php echo $activo ?></td>
                                                    <td class="text-center">
                                                    </a>
                                                        <a class="btn btn-info " href="./../../../FUNCTIONS/CRUD/update_adviser.php?ida=<?= $row['id_log'] ?>" data-toggle="modal" data-target="#modal-sm<?= $row['id_log'] ?>"><i class="material-icons">person</i></a>
                                              
                                                        <a class="btn btn-success bg-success" href="./../../../FUNCTIONS/CRUD/updateAdviser.php?id=<?= $row['id_user'] ?>" data-toggle="modal" data-target="#update<?= $row['id_user'] ?>"><i class="material-icons">edit</i></a>

                                                        <a type="submit" href="./../../../FUNCTIONS/CRUD/delete_adviser.php?id=<?= $row['id_log'] ?>" class="btn btn-danger bg-danger" data-toggle="modal" data-target="#delete<?= $row['id_user'] ?>"><i class="material-icons">close</i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>